package unittestings

import (
	"errors"
	"fmt"
)

//PrintPrime function declaration
func PrintPrime(number int) (isPrime bool, err error) {
	isPrime = true

	if number == 0 || number == 1 {
		fmt.Printf(" %d is not a  prime number\n", number)
		return isPrime, errors.New("is not a  prime number")
	} else {
		for i := 2; i <= number/2; i++ {
			if number%i == 0 {
				fmt.Printf(" %d is not a  prime number\n", number)
				isPrime = false
				err = errors.New("is not a  prime number")
				break

			}
		}
		if isPrime == true {
			fmt.Printf(" %d is a prime number\n", number)

		}
	}
	return isPrime, err
}
