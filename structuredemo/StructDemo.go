package structuredemo

import "fmt"

//Emp declared the structure named emp
type Emp struct {
	Name    string
	Address string
	Age     int
}

//Display function which accepts variable of emp type and prints name property
func (e Emp) Display() {
	fmt.Println(e.Name)
}
