package arraydemo

import "fmt"

//ArrayDemo Function defination
func ArrayDemo() {
	a := [...]string{"1", "2", "3", "4", "5"}

	//	Array declaration can be either
	/*var a []string
	a[0]="1"
	a[1]="2"
	a[2]="3"
	*/
	sliceA := a[1:3]
	b := [5]string{"one", "two", "three", "four", "five"}
	sliceB := b[1:3]

	fmt.Println("SliceA:", sliceA)
	fmt.Println("SliceB:", sliceB)
	fmt.Println("Length of sliceA:", len(sliceA))
	fmt.Println("Length of sliceB:", len(sliceB))

	sliceA = append(sliceA, sliceB...) // appending slice
	fmt.Println("New SliceA after appending sliceB :", sliceA)

	sliceA = append(sliceA, "text1") // appending value
	fmt.Println("New SliceA after appending text1 :", sliceA)
}
