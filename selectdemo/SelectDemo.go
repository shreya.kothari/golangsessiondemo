package selectdemo

import "time"

//Data1 push data to channel with a 4 second delay
func Data1(ch chan string) {
	time.Sleep(4 * time.Second)
	ch <- "from data1()"
}

//Data2 push data to channel with a 2 second delay
func Data2(ch chan string) {
	time.Sleep(2 * time.Second)
	ch <- "from data2()"
}
