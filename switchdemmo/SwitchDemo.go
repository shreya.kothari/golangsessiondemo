package switchdemmo

import "fmt"

//SwitchDemo function
func SwitchDemo() {
	a, b := 2, 1

	switch a + b {
	case 1:
		fmt.Println("Sum is 1 got from switch")
	case 2:
		fmt.Println("Sum is 2 got from switch")
	case 3:
		fmt.Println("Sum is 3 got from switch")
	default:
		fmt.Println("Printing default got from switch")
	}
}
