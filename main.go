package main

import (
	"GoLangDemos/interfaceS"
	"GoLangDemos/pointerdemo"
	"GoLangDemos/returntypefunction"
	"GoLangDemos/selectdemo"
	"GoLangDemos/structuredemo"
	"GoLangDemos/unittestings"
	"fmt"
)

func main() {
	//loopdemo.Loop()
	//variabledemo.Variable()
	//defer switchdemmo.SwitchDemo()
	//defer arraydemo.ArrayDemo()
	result, err := unittestings.PrintPrime(101)
	if err == nil {
		fmt.Println(result)
	}
	x, y := 15, 10

	//calls the function calc with x and y an d gets sum, diff as output
	sum, diff := returntypefunction.ReturnTypeFunc(x, y)
	fmt.Println("Sum", sum)
	fmt.Println("Diff", diff)

	pointerdemo.PointerDemo()

	// declares a variable, empdata1, of the type Emp
	var empdata1 structuredemo.Emp
	//assign values to members of empdata1
	empdata1.Name = "John"
	empdata1.Address = "Street-1, London"
	empdata1.Age = 30

	//declares and assign values to variable empdata2 of type Emp
	empdata2 := structuredemo.Emp{"Raj", "Building-1, Paris", 25}

	//prints the member name of empdata1 and empdata2 using display function
	/*structuredemo.Display(empdata1)
	structuredemo.Display(empdata2) */
	//Invoking the method using the receiver of the type emp
	// syntax is variable.methodname()
	empdata1.Display()
	empdata2.Display()

	//creating channel variables for transporting string values
	chan1 := make(chan string)
	chan2 := make(chan string)

	//invoking the subroutines with channel variables
	go selectdemo.Data1(chan1)
	go selectdemo.Data2(chan2)

	//Both case statements wait for data in the chan1 or chan2.
	//chan2 gets data first since the delay is only 2 sec in data2().
	//So the second case will execute and exits the select block
	select {
	case x := <-chan1:
		fmt.Println(x)
	case y := <-chan2:
		fmt.Println(y)
	}

	r := interfaceS.Rect{Width: 3, Height: 4}
	c := interfaceS.Circle{Radius: 5}

	interfaceS.Measure(r)
	interfaceS.Measure(c)

}
