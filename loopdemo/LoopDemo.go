package loopdemo

import "fmt"

//Loop function defination
func Loop() {
	var i int
	for i = 1; i <= 10; i++ {
		if i < 5 {
			//Executes if i is less than 5
			fmt.Println("i is less than 5")
		} else if i >= 5 && i <= 10 {
			//Executes if i >= 5 and i<=10
			fmt.Println("i is between 10 and 90")
		} else {
			//Executes if both above cases fail i.e i>10
			fmt.Println("i is greater than 10")
		}
	}
}
